# Laravel Basicos
Projeto simples utilizando o laravel 8 seguindo os ensinamentos do curso **[Master Laravel for Beginners & Intermediate [2020 Update]](https://www.udemy.com/course/laravel-beginner-fundamentals/learn/lecture/23329924#overview)**

## 1. Requerimentos
    - PHP 7.4
    - Laravel 8
    - MySQL
    - NPM
## 2. Instalação

```
$ git clone https://gitlab.com/lucasacoutinho/laravel-basicos.git
```

```
$ composer i
```
```
$ npm i

```
```
$ cp .env.example .env
```

```
$ php artisan key:generate
```

```
$ php artisan migrate
```

```
$ php artisan db:seed -n
```

## 3. Utilização

```
$ php artisan serve
```
```
$ npm run dev
```

## 4. Usuario
    - email   : master@admin.co,
    - password: password

# 5. Links úteis

- **[Laravel](http://laravel.com/)**
- **[Bootstrap 4](https://getbootstrap.com/docs/4.0/getting-started/introduction/)**
- **[Laravel IDE Helper](https://github.com/barryvdh/laravel-ide-helper)**
- **[Commits Semânticos](https://ildaneta.dev/posts/entendo-a-import%C3%A2ncia-dos-commits-sem%C3%A2nticos/)**
- **[Melhores práticas para o Laravel (Laravel Best Practices)](https://github.com/alexeymezenin/laravel-best-practices)**
