<?php

namespace App\Http\Controllers;

use App\Events\CommentPosted;
use App\Models\Comment;
use App\Models\BlogPost;
use App\Jobs\ThrottledMail;
use App\Mail\CommentPostedMarkdown;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Jobs\NotifyUsersPostWasCommented;
use App\Http\Requests\Comment\StoreRequest as CommentStore;
use App\Http\Requests\Comment\UpdateRequest as CommentUpdate;

class CommentController extends Controller
{
    public function store(BlogPost $post, CommentStore $request)
    {
        $comment = $post->comments()->create([
            'email'   => $request->email,
            'content' => $request->content,
            'user_id' => Auth::id(),
        ]);

        event(new CommentPosted($comment));

        return redirect()->back()->with('status', 'Comentário adicionado com sucesso!');
    }

    public function update(CommentUpdate $request, BlogPost $post, Comment $comment)
    {
        $comment->update($request->validated());

        return redirect()->back()->with('status', 'Comentário atualizado com sucesso!');
    }

    public function destroy(BlogPost $post, Comment $comment)
    {
        $this->authorize('comment:destroy', $comment);

        $comment->delete();
        
        return redirect()->back()->with('status', 'Comentário removido com sucesso!');
    }
}
