<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home() {
        return view('pages.index', ['active' => 'home']);
    }

    public function about() {
        return view('pages.about', ['active' => 'about']);
    }
}
