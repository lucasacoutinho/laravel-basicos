<?php

namespace App\Http\Controllers;

use Storage;
use App\Models\User;
use App\Models\BlogPost;
use Domain\Posts\ImageStorage;
use Domain\Posts\VisitCounter;
use App\Events\BlogPostCreated;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use App\Http\Requests\Post\StoreRequest as PostStore;
use App\Http\Requests\Post\UpdateRequest as PostUpdate;

class PostController extends Controller
{
    private const IMG_DIR = 'thumbnail';

    public function __construct()
    {
        $this->middleware('auth')->only(['create', 'store', 'edit', 'update', 'destroy']);
    }

    public function index()
    {
        $posts = BlogPost::with('tags')->orderBy('created_at', 'desc')->paginate(7);

        return view('posts.index', [
            'posts' => $posts,
            'most_commented' => Cache::remember('most-commented-posts', now()->addSeconds(120), fn () => BlogPost::mostCommented()->take(5)->get()),
            'most_active_users' => Cache::remember('most-active-users', now()->addSeconds(120), fn () => User::withMostBlogPostsLastMonth()->take(5)->get()),
            'active' => 'posts'
        ]);
    }

    public function create()
    {
        return view('posts.create', ['active' => 'posts']);
    }

    public function store(PostStore $request)
    {
        $post = Auth::user()->posts()->create($request->validated());

        (new ImageStorage(self::IMG_DIR))->verify($request)->store($post);
        

        event(new BlogPostCreated($post));

        $request->session()->flash('status', 'Postagem criada com sucesso!');
        return redirect()->route('post.show', ['post' => $post]);
    }

    public function show(BlogPost $post)
    {
        $post->load(['comments', 'tags', 'user', 'comments.user', 'comments.tags']);
        return view('posts.show', ['post' => $post, 'active' => 'posts', 'counter' => (new VisitCounter)->counter($post->id)]);
    }

    public function edit(BlogPost $post)
    {
        $this->authorize('posts:update', $post);

        return view('posts.edit', ['post' => $post, 'active' => 'posts']);
    }

    public function update(PostUpdate $request, BlogPost $post)
    {
        $post->update($request->validated());

        (new ImageStorage(self::IMG_DIR))->verify($request)->update($post);

        $request->session()->flash('status', 'Postagem atualizada com sucesso!');
        return redirect()->route('post.show', ['post' => $post, 'active' => 'posts']);
    }

    public function destroy(BlogPost $post)
    {
        $this->authorize('posts:destroy', $post);

        $post->delete();
        return redirect()->route('post.index', ['post' => $post, 'active' => 'posts'])->with('status', "Postagem $post->title excluida com sucesso!");
    }
}
