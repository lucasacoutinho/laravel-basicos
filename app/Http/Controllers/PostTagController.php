<?php

namespace App\Http\Controllers;

use App\Models\Tag;
use App\Http\Controllers\Controller;

class PostTagController extends Controller
{
    public function index(Tag $tag) {
        return view('posts.index', [
            'posts' => $tag->posts()->with('tags')->paginate(5),
            'active' => 'posts'
        ]);
    }
}
