<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Comment\StoreRequest as CommentStore;
use App\Models\User;

class UserCommentController extends Controller
{
    public function store(User $user, CommentStore $request)
    {
        $user->commentsOn()->create([
            'email'   => $request->email,
            'content' => $request->content,
            'user_id' => Auth::id(),
        ]);
        
        return redirect()->back()->with('status', 'Comentário adicionado com sucesso!');
    }
}
