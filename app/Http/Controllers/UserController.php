<?php

namespace App\Http\Controllers;

use App\Models\User;
use Domain\Posts\ImageStorage;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\UpdateRequest as UserUpdate;

class UserController extends Controller
{
    private const IMG_DIR = 'avatar';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show(User $user)
    {
        $this->authorize('user:view', $user);
        $user->load('commentsOn');
        return view('users.profile.show', compact('user'));
    }

    public function edit(User $user)
    {
        $this->authorize('user:update', $user);
        return view('users.profile.edit', compact('user'));
    }

    public function update(UserUpdate $request, User $user)
    {
        (new ImageStorage(self::IMG_DIR))->verify($request)->update($user);

        return redirect()->back()->with('status', 'Profile image was updated');
    }
}
