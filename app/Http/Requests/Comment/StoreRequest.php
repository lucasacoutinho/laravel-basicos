<?php

namespace App\Http\Requests\Comment;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    public function authorize()
    {
        return Auth::check();
    }

    public function rules()
    {
        return [
            'email'   => ['required', 'email'],
            'content' => ['required'],
        ];
    }
}
