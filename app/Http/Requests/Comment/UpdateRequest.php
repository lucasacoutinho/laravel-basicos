<?php

namespace App\Http\Requests\Comment;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('comment:update', $this->route('comment'));
    }

    public function rules()
    {
        return [
            'content' => ['filled', 'min:20']
        ];
    }
}
