<?php

namespace App\Http\Requests\Post;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class StoreRequest extends FormRequest
{
    public function authorize()
    {
        return Auth::check();
    }

    public function rules()
    {
        return [
            'title'     => ['required', 'max:255'], 
            'content'   => ['required', 'min:20'],
            'thumbnail' => ['image', 'mimes:jpg,jpeg', 'max:1024', 'dimensions:width=400,height=300'],
        ];
    }
}
