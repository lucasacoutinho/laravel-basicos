<?php

namespace App\Http\Requests\Post;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('posts:update', $this->route('post'));
    }

    public function rules()
    {
        return [
            'title'     => ['filled', 'max:255'], 
            'content'   => ['filled', 'min:20'],
            'thumbnail' => ['image', 'mimes:jpg,jpeg', 'max:1024', 'dimensions:width=400,height=300'],
        ];
    }
}
