<?php

namespace App\Http\Requests\User;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('user:update', $this->route('user'));
    }

    public function rules()
    {
        return [
            'avatar' => ['image', 'mimes:jpg,jpeg,png', 'max:1024', 'dimensions:width=128,height=128'],
        ];
    }
}
