<?php

namespace App\Jobs;

use App\Models\User;
use App\Models\Comment;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use App\Mail\CommentPostedOnPostWatched;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class NotifyUsersPostWasCommented implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public Comment $comment;

    public function __construct(Comment $comment)
    {
        $this->comment = $comment;
    }

    public function handle()
    {
        User::commentedOnPost($this->comment->commentable)->get()->filter(function (User $user){
            return $user->id !== $this->comment->user->id;
        })->map(function(User $user){
            ThrottledMail::dispatch(new CommentPostedOnPostWatched($this->comment, $user), $user);
        });
    }
}
