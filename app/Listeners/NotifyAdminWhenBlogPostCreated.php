<?php

namespace App\Listeners;

use App\Models\User;
use App\Jobs\ThrottledMail;
use App\Mail\BlogPostAdded;
use App\Events\BlogPostCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyAdminWhenBlogPostCreated
{
    public function handle(BlogPostCreated $event)
    {
        User::whereIsAnAdmin()->get()->map(function(User $user){
            ThrottledMail::dispatch(new BlogPostAdded(), $user);
        });
    }
}
