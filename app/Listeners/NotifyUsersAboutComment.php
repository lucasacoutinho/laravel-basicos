<?php

namespace App\Listeners;

use App\Jobs\ThrottledMail;
use App\Events\CommentPosted;
use App\Mail\CommentPostedMarkdown;
use Illuminate\Queue\InteractsWithQueue;
use App\Jobs\NotifyUsersPostWasCommented;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyUsersAboutComment
{
    public function handle(CommentPosted $event)
    {
        ThrottledMail::dispatch(
            new CommentPostedMarkdown(
                $event->comment->load(['user', 'user.image', 'commentable', 'commentable.user'])), 
                $event->comment->commentable->user
        )->onQueue('low');
                        
        NotifyUsersPostWasCommented::dispatch($event->comment)->onQueue('high');    
    }
}
