<?php

namespace App\Mail;

use App\Models\Comment;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;
use Illuminate\Contracts\Queue\ShouldQueue;

class CommentPosted extends Mailable
{
    use Queueable, SerializesModels;

    public Comment $comment;

    public function __construct(Comment $comment)
    {
        $this->comment = $comment;
    }
    
    public function build()
    {
        // return $this->attachFromStorageDisk('public', $this->comment->user->image->path, 'profile_picture', ['mime' => 'image/jpeg'])
        //             ->subject("Comment was posted on your {$this->comment->commentable->title} blog post")
        //             ->view('emails.posts.commented');

        return $this->attachData(Storage::get($this->comment->user->image->path), 'profile_picture.jpeg', ['mime' => 'image/jpeg'])
                    ->subject("Comment was posted on your {$this->comment->commentable->title} blog post")
                    ->view('emails.posts.commented');
    }
}
