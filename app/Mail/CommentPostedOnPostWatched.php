<?php

namespace App\Mail;

use App\Models\User;
use App\Models\Comment;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CommentPostedOnPostWatched extends Mailable
{
    use Queueable, SerializesModels;

    public Comment $comment;
    public User $user;
    
    public function __construct(Comment $comment, User $user)
    {
        $this->comment = $comment;
        $this->user = $user;
    }

    public function build()
    {
        return $this->markdown('emails.posts.comment-posted-on-watched');
    }
}
