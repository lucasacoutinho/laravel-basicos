<?php

namespace App\Models;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Traits\Taggable;

class BlogPost extends Model
{
    use HasFactory, SoftDeletes, Taggable;

    protected $fillable = [
        'title',
        'content'
    ];

    public static function boot() {
        static::addGlobalScope('withDeleted', function(Builder $builder) {
           if(Auth::check() && Auth::user()->is_admin){
               $builder->withTrashed();
           }
        });

        parent::boot();
        
        // static::deleting(function(BlogPost $post){
        //     $post->comments()->delete();
        // });

        // static::restoring(function(BlogPost $post){
        //     $post->comments()->restore();
        // });
    }

    public function scopeMostCommented(Builder $builder) {
        return $builder->withCount('comments')->orderBy('comments_count', 'desc');
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function comments() {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function image() {
        return $this->morphOne(Image::class, 'imageable');
    }
}
