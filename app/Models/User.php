<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function scopeWithMostBlogPosts(Builder $builder) {
        return $builder->withCount('posts')->orderBy('posts_count', 'desc');
    }

    public function scopeWithMostBlogPostsLastMonth(Builder $builder) {
        return $builder->withCount(['posts' => function($query) {
            $query->whereBetween(static::CREATED_AT, [now()->subMonths(1), now()]);
        }])->has('posts', '>=', 1)
        ->orderBy('posts_count', 'desc');
    }

    public function scopeCommentedOnPost(Builder $builder, BlogPost $post) {
        return $builder->whereHas('comments', function($query) use ($post){
            return $query->where('commentable_id', $post->id)->where('commentable_type', BlogPost::class);
        });
    }
    
    public function scopeWhereIsAnAdmin(Builder $builder) {
        return $builder->where('is_admin', true);
    }

    public function posts() {
        return $this->hasMany(BlogPost::class);
    }

    public function comments() {
        return $this->hasMany(Comment::class);
    }

    public function commentsOn() {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function image() {
        return $this->morphOne(Image::class, 'imageable');
    }
}