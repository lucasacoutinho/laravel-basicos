<?php

namespace App\Providers;

use App\Models\BlogPost;
use App\View\Components\Alert;
use App\View\Components\Error;
use App\View\Components\Button;
use App\Observers\BlogPostObserver;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public function register()
    {
        //
    }

    public function boot()
    {
        Paginator::useBootstrap();
        Schema::defaultStringLength(191);
        Blade::component('button', Button::class);
        Blade::component('alert', Alert::class);
        Blade::component('error', Error::class);

        BlogPost::observe(BlogPostObserver::class);
    }
}
