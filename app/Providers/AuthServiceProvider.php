<?php

namespace App\Providers;

use App\Policies\CommentPolicy;
use App\Policies\BlogPostPolicy;
use App\Policies\UserPolicy;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    public function boot()
    {
        $this->registerPolicies();

        Gate::define('about:show', function($user){
            return $user->is_admin;
        });

        Gate::define('secret:show', function($user){
            return $user->is_admin;
        });


        Gate::define('posts:update',  [BlogPostPolicy::class, 'update']);
        Gate::define('posts:destroy', [BlogPostPolicy::class, 'delete']);

        Gate::define('comment:update',  [CommentPolicy::class, 'update']);
        Gate::define('comment:destroy', [CommentPolicy::class, 'delete']);

        Gate::define('user:viewAny', [UserPolicy::class, 'viewAny']);
        Gate::define('user:view',    [UserPolicy::class, 'view']);
        Gate::define('user:create',  [UserPolicy::class, 'create']);
        Gate::define('user:update',  [UserPolicy::class, 'update']);
        Gate::define('user:destroy', [UserPolicy::class, 'delete']);

        // Gate::before(function($user, $ability) {
        //     if($user->is_admin) {
        //         return true;
        //     }
        // });

        // Gate::after(function($user, $ability, $result) {
        //     return $result;
        // });
    }
}
