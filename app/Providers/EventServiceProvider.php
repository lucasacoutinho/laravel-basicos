<?php

namespace App\Providers;

use App\Listeners\CacheSubscriber;
use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use App\Events\{CommentPosted, BlogPostCreated};
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use App\Listeners\{NotifyUsersAboutComment, NotifyAdminWhenBlogPostCreated};
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        CommentPosted::class => [
            NotifyUsersAboutComment::class
        ],
        BlogPostCreated::class => [
            NotifyAdminWhenBlogPostCreated::class
        ]
    ];

    protected $subscribe = [
        CacheSubscriber::class
    ];

    public function boot()
    {
        //
    }
}
