<?php

namespace Database\Factories;

use App\Models\BlogPost;
use Illuminate\Database\Eloquent\Factories\Factory;

class BlogPostFactory extends Factory 
{
    protected $model = BlogPost::class;

    public function definition()
    {
        return [
            'title'   => $this->faker->words(5, true),
            'content' => $this->faker->text(500),
        ];
    }
}
