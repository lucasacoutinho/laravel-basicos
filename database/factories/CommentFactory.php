<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Comment;

class CommentFactory extends Factory
{
    protected $model = Comment::class;

    public function definition()
    {
        return [
            'email'   => $this->faker->safeEmail(),
            'content' => $this->faker->text(300),
        ];
    }
}
