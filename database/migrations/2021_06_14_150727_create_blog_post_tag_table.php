<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlogPostTagTable extends Migration
{
    public function up()
    {
        Schema::create('blog_post_tag', function (Blueprint $table) {
            $table->id();
            $table->foreignId('post_id')
                  ->contrained('blog_posts')
                  ->onUpdate('CASCADE')
                  ->onDelete('CASCADE');

            $table->foreignId('tag_id')
                  ->contrained('tags')
                  ->onUpdate('CASCADE')
                  ->onDelete('CASCADE');
                  
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('blog_post_tag');
    }
}
