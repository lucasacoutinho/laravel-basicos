<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPolymorphToCommentsTable extends Migration
{
    public function up()
    {
        Schema::table('comments', function (Blueprint $table) {
            $table->dropForeign(['blog_post_id']);
            $table->dropColumn('blog_post_id');

            $table->morphs('commentable');
        });
    }

    public function down()
    {
        Schema::table('comments', function (Blueprint $table) {
            $table->foreignId('blog_post_id')->index()->nullable()->constrained('blog_posts')->onUpdate('cascade')->onDelete('cascade');
            $table->dropMorphs('commentable');
        });
    }
}
