<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameBlogPostTagTableToTaggables extends Migration
{
    public function up()
    {
        Schema::table('blog_post_tag', function (Blueprint $table) {
            $table->dropColumn('post_id');
        });

        Schema::rename('blog_post_tag', 'taggables');

        Schema::table('taggables', function (Blueprint $table) {
            $table->morphs('taggable');
        });
    }

    public function down()
    {
        Schema::table('taggables', function (Blueprint $table) {
            $table->dropMorphs('taggable');
        });

        Schema::rename('taggables', 'blog_post_tag');

        Schema::table('blog_post_tag', function (Blueprint $table) {
            $table->foreignId('post_id')->index()->contrained('blog_posts')->onUpdate('CASCADE')->onDelete('CASCADE');        
        });
    }
}
