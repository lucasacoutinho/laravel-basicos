<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\BlogPost;
use Illuminate\Database\Seeder;

class BlogPostSeeder extends Seeder
{
    public function run()
    {
        BlogPost::factory(20)->for(User::inRandomOrder()->first())->create();
    }
}

