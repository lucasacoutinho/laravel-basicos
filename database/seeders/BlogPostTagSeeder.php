<?php

namespace Database\Seeders;

use App\Models\Tag;
use App\Models\BlogPost;
use Illuminate\Database\Seeder;

class BlogPostTagSeeder extends Seeder
{
    public function run()
    {
        if(!Tag::all()->count()){
            $this->command->info('No tags found, skipping assigning tags to blog posts');
            return;
        }
        
        BlogPost::all()->each(function(BlogPost $post) {
            $tags = Tag::inRandomOrder()->take(random_int(1, 5))->get()->pluck('id');
            $post->tags()->sync($tags);
        });
    }
}
