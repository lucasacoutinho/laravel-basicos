<?php

namespace Database\Seeders;

use App\Models\BlogPost;
use App\Models\User;
use App\Models\Comment;
use Illuminate\Database\Seeder;

class CommentSeeder extends Seeder
{
    public function run()
    {
        Comment::factory(15)->make()->each(function($comment) {
            $comment->commentable_id = BlogPost::inRandomOrder()->first()->id;
            $comment->commentable_type = 'App\Models\BlogPost';
            $comment->user()->associate(User::inRandomOrder()->first());
            $comment->save();
        });

        Comment::factory(15)->make()->each(function($comment) {
            $comment->commentable_id = User::inRandomOrder()->first()->id;
            $comment->commentable_type = 'App\Models\User';
            $comment->user()->associate(User::inRandomOrder()->first());
            $comment->save();
        });
    }
}
