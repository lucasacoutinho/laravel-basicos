<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {       
        $this->refresh();
        
        $this->call([
            UserSeeder::class,
            BlogPostSeeder::class,
            CommentSeeder::class,
            TagSeeder::class,
            BlogPostTagSeeder::class,
        ]);
    }

    private function refresh() {
        if($this->command->confirm('Do you want to refresh the database ?')) {
            $this->command->call('migrate:refresh');
            $this->command->info('Database refreshed successfully.');
        }
    }
}
