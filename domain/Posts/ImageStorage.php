<?php

namespace Domain\Posts;

use App\Models\Image;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class ImageStorage
{
    private $file;
    private $path;

    public function __construct(string $path) {
        $this->path = $path;
    }

    public function verify(Request $request): ImageStorage
    {
        if ($request->hasFile($this->path)) {
            $this->file = $request->file($this->path);
        }

        return $this;
    }

    private function name($id, $file): string
    {
        return $id . '.' . $file->guessExtension();
    }

    private function path(Model $model): string
    {
        return $this->file->storeAs($this->path, $this->name($model->id, $this->file));
    }

    public function store(Model $model): void
    {
        if (!$this->file) {
            return;
        }

        $model->image()->save(Image::make(['path' => $this->path($model)]));
    }

    public function update(Model $model): void
    {
        if (!$model->image) {
            $this->store($model);
            return;
        }

        Storage::delete($model->image->path);

        if (!$this->file) {
            $model->image->delete();
            return;
        }

        $model->image->update(['path' => $this->path($model)]);
    }
}
