<?php

namespace Domain\Posts;

use Illuminate\Support\Facades\Cache;

class VisitCounter {
    public function counter(int $identifier) {
        $sessionID = session()->getId();
        $counterKey = "blog-post-{$identifier}-counter";
        $userKey = "blog-post-{$identifier}-users";

        $users = Cache::get($userKey, []);
        $usersUpdate = [];
        $difference = 0;

        foreach($users as $session => $lastVisit) {
            if(now()->diffInMinutes($lastVisit) >= 1) {
                $difference--;
            } else {
                $usersUpdate[$session] = $lastVisit;
            }
        }

        if(!array_key_exists($sessionID, $users) || now()->diffInMinutes($users[$sessionID]) >= 1){
            $difference++;
        }

        $usersUpdate[$sessionID] = now();
        Cache::forever($userKey, $usersUpdate);
        if(!Cache::has($counterKey)) {
            Cache::forever($counterKey, 1);
        } else {
            Cache::increment($counterKey, $difference);
        }

        return Cache::get($counterKey);
    }
}