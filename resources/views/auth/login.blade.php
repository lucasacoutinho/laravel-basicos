@extends('layouts.master')
@section('pagina', 'Login')

@section('content')
    <div class="container bg-white p-4 rounded shadow-sm align-items-center justify-content-center" style="max-width: 540px;">
        <h1 class="text-center display-4">Sign in</h1>
        <form method="POST" action="{{route('login')}}">
            @csrf
           <div class="form-group">
                <div class="input-group mb-3">
                    <span class="input-group-text" id="email-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-envelope" viewBox="0 0 16 16">
                            <path d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4zm2-1a1 1 0 0 0-1 1v.217l7 4.2 7-4.2V4a1 1 0 0 0-1-1H2zm13 2.383-4.758 2.855L15 11.114v-5.73zm-.034 6.878L9.271 8.82 8 9.583 6.728 8.82l-5.694 3.44A1 1 0 0 0 2 13h12a1 1 0 0 0 .966-.739zM1 11.114l4.758-2.876L1 5.383v5.73z"/>
                          </svg>
                    </span>
                    <input type="text" class="form-control" name="email" placeholder="E-mail" aria-label="email" aria-describedby="email-icon">
                    @if ($errors->has('email'))
                        <small class="text-muted invalid-feedback">{{$errors->first('email')}}</small>
                    @endif
                </div>
           </div>
           <div class="form-group">
            <div class="input-group mb-3">
                <span class="input-group-text" id="password-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-key-fill" viewBox="0 0 16 16">
                        <path d="M3.5 11.5a3.5 3.5 0 1 1 3.163-5H14L15.5 8 14 9.5l-1-1-1 1-1-1-1 1-1-1-1 1H6.663a3.5 3.5 0 0 1-3.163 2zM2.5 9a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/>
                      </svg>
                </span>
                <input type="password" class="form-control" name="password" placeholder="Senha" aria-label="password" aria-describedby="password-icon">
                @if ($errors->has('password'))
                    <small class="text-muted invalid-feedback">{{$errors->first('password')}}</small>
                @endif
            </div>
           </div>
           <div class="form-check my-2">
               <input type="checkbox" class="form-check-input" name="remember" id="remember" value="checkedValue" {{old('remember') ? 'checked': ''}}>
               <label class="form-check-label  font-weight-bold" for="remember">Remember-me</label>
           </div>
           <x-button type="submit" :message="'Login'" class="btn-block"/>
        </form>
    </div>
@endsection