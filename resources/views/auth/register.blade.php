@extends('layouts.master')
@section('pagina', 'Cadastro')

@section('content')
<div class="container bg-white p-3 rounded shadow-sm" style="max-width: 540px;">
  <h1 class="text-center display-4">Sign up</h1>
  <form method="POST" action="{{route('register')}}">
    @csrf
    <div class="form-group">
      <div class="input-group mb-3">
        <span class="input-group-text" id="name-icon">
          <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person"
            viewBox="0 0 16 16">
            <path
              d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0zm4 8c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z" />
          </svg>
        </span>
        <input type="text" name="name" class="form-control {{$errors->has('name') ? 'is-invalid' : ''}}"
          placeholder="Name" aria-label="name" value="{{old('name')}}" aria-describedby="name-icon" required>
        @if ($errors->has('name'))
        <small class="text-muted invalid-feedback">{{$errors->first('name')}}</small>
        @endif
      </div>
    </div>

    <div class="form-group">
      <div class="input-group mb-3">
        <span class="input-group-text" id="email-icon">
          <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-envelope"
            viewBox="0 0 16 16">
            <path
              d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4zm2-1a1 1 0 0 0-1 1v.217l7 4.2 7-4.2V4a1 1 0 0 0-1-1H2zm13 2.383-4.758 2.855L15 11.114v-5.73zm-.034 6.878L9.271 8.82 8 9.583 6.728 8.82l-5.694 3.44A1 1 0 0 0 2 13h12a1 1 0 0 0 .966-.739zM1 11.114l4.758-2.876L1 5.383v5.73z" />
          </svg>
        </span>
        <input type="text" name="email" class="form-control {{$errors->has('email') ? 'is-invalid' : ''}}"
          placeholder="E-mail" aria-label="email" value="{{old('email')}}" aria-describedby="email-icon" required>
        @if ($errors->has('email'))
        <small class="text-muted invalid-feedback">{{$errors->first('email')}}</small>
        @endif
      </div>
    </div>

    <div class="form-group">
      <div class="input-group mb-3">
        <span class="input-group-text" id="password-icon">
          <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-key"
            viewBox="0 0 16 16">
            <path
              d="M0 8a4 4 0 0 1 7.465-2H14a.5.5 0 0 1 .354.146l1.5 1.5a.5.5 0 0 1 0 .708l-1.5 1.5a.5.5 0 0 1-.708 0L13 9.207l-.646.647a.5.5 0 0 1-.708 0L11 9.207l-.646.647a.5.5 0 0 1-.708 0L9 9.207l-.646.647A.5.5 0 0 1 8 10h-.535A4 4 0 0 1 0 8zm4-3a3 3 0 1 0 2.712 4.285A.5.5 0 0 1 7.163 9h.63l.853-.854a.5.5 0 0 1 .708 0l.646.647.646-.647a.5.5 0 0 1 .708 0l.646.647.646-.647a.5.5 0 0 1 .708 0l.646.647.793-.793-1-1h-6.63a.5.5 0 0 1-.451-.285A3 3 0 0 0 4 5z" />
            <path d="M4 8a1 1 0 1 1-2 0 1 1 0 0 1 2 0z" />
          </svg>
        </span>
        <input type="password" name="password" class="form-control {{$errors->has('password') ? 'is-invalid' : ''}}"
          placeholder="Senha" aria-label="password" value="{{old('password')}}" aria-describedby="password-icon"
          required>
        @if ($errors->has('password'))
        <small class="text-muted invalid-feedback">{{$errors->first('password')}}</small>
        @endif
      </div>
    </div>

    <div class="form-group">
      <div class="input-group mb-3">
        <span class="input-group-text" id="password-confirmation">
          <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-key-fill"
            viewBox="0 0 16 16">
            <path
              d="M3.5 11.5a3.5 3.5 0 1 1 3.163-5H14L15.5 8 14 9.5l-1-1-1 1-1-1-1 1-1-1-1 1H6.663a3.5 3.5 0 0 1-3.163 2zM2.5 9a1 1 0 1 0 0-2 1 1 0 0 0 0 2z" />
          </svg>
        </span>
        <input type="password" name="password_confirmation" class="form-control" placeholder="Senha"
          aria-label="password_confirmation" aria-describedby="password-confirmation-icon" required>
      </div>
    </div>
    <x-button type="submit" :message="'Register'" class="btn-block"/>
  </form>
</div>
@endsection