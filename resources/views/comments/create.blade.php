@csrf
<h2>Adicione um comentário</h2>
<div class="form-group">
    <label for="exampleInputEmail1" class="text-weight-bold">Endereço de Email:</label>
    <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
        placeholder="Insira um email">
    <small id="emailHelp" class="form-text text-muted">Seu email será mantido privado</small>
</div>
<div class="form-group">
    <label for="content" class="text-weight-bold">Conteúdo:</label>
    <textarea class="form-control" name="content" id="content" minlength="20" rows="3"></textarea>
</div>
<x-button type="submit" :message="'Comment'" class="btn-block" />