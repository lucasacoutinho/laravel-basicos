<form id="form-delete" class="invisible" method="POST"
    action="{{route('comment.post.destroy', ['post' => $post->id, 'comment' => $item->id])}}">
    @method('DELETE')
    @csrf
</form>