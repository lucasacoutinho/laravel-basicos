<div class="shadow-sm bg-white p-3 rounded m-3">
    <h2>Comentários recentes</h2>
    @foreach ($post->comments as $item)
        @include('comments.partials.comment')
    @endforeach
</div>