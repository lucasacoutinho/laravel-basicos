<div class="d-flex flex-row comment-row m-t-0 bg-light rounded m-1 p-3">
    <div class="comment-text w-100">
        <div>
            <h6 class="font-medium">
                <strong>{{$item->user->name}}</strong> - <span class="font-italic">{{$item->email}}</span>
                <p>Ultima atualização, {{$item->updated_at->diffForHumans()}}</p>
                @foreach ($item->tags as $tag)
                    <span class="badge badge-success badge-lg">{{$tag->name}}</span>                
                @endforeach
            </h6>
            <p class="m-b-15 d-block">{{$item->content}}</p>
            <div class="comment-footer"><span class="text-muted float-right">
                Criado, {{$item->created_at->diffForHumans()}}</span>
            </div>
        </div>
        <div>
            @if ($item->commentable_type == 'App\Models\BlogPost')
                @auth
                    @can('comment:update', $item)
                        @include('comments.update')
                        <a class="comment-edit-btn btn btn-secondary btn-sm" onclick="event.preventDefault(); event.target.closest('div').querySelector('#comment-edit-form').classList.toggle('ocult')">Editar</a>
                    @endcan
                    @can('comment:destroy', $item)
                        <a id="btn-delete" href="#" type="button" class="btn btn-danger btn-sm" onclick="event.preventDefault(); event.target.closest('div').querySelector('#form-delete').submit();">Deletar</a>
                        @include('comments.delete')
                    @endcan
                @endauth
            @endif
        </div>
    </div>
</div>