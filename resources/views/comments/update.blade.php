<div id="comment-edit-form" class="ocult">
    <form method="POST" action="{{route('comment.post.update', ['post' => $post->id, 'comment' => $item->id])}}">
        @method('PUT')
        @csrf
        <div class="form-group">
            <label for="content">Conteúdo</label>
            <textarea class="form-control" name="content" id="content" rows="3" minlength="20" required></textarea>
        </div>

        <x-button type="submit" :message="'Save'" class="right"/>
    </form>
    <br>
</div>