@if(session('status'))
    <div class="alert alert-success alert-dismissible fade show shadow-sm p-3 rounded m-3" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            <span class="sr-only">Close</span>
        </button>
        {{$message}}
    </div>
@endif
