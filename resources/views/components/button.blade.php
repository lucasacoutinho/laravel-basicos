<div>
    <button class="btn btn-primary {{ $attributes->get('class') }}" {{ $attributes }}>    
        {{$message}}
    </button>
</div>