@component('mail::message')
# Comment was posted on post you're watching

Hi {{ $user->name }}

@component('mail::panel')
    {{ $comment->content }}
@endcomponent

@component('mail::button', ['url' => route('post.show', ['post' => $comment->commentable->id]) ])
View blog post
@endcomponent

@component('mail::button', ['url' => route('users.show', ['user' => $comment->user->id]) ])
Visit {{ $comment->user->name }} profile
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
