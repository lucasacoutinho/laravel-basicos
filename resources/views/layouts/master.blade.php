<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('pagina')</title>
    <link rel="stylesheet" href="{{mix('css/app.css')}}">
    <script src="{{mix('js/app.js')}}" defer></script>
    <style>
        .ocult {
            display: none;
        }
    </style>
</head>

<body>
    <header class="mb-3">
        <nav class="navbar navbar-expand-md navbar-dark bg-dark">
            <a class="navbar-brand" href="#">B-Logger</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse" style="float: right;">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item @if(isset($active) && $active == 'home')active @endif">
                        <a class="nav-link" href="{{route('home.index')}}">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item @if(isset($active) && $active == 'posts')active  @endif">
                        <a class="nav-link" href="{{route('post.index')}}">Postagens</a>
                    </li>
                    <li class="nav-item @if(isset($active) && $active == 'about')active @endif">
                        <a class="nav-link" href="{{route('home.about')}}">Sobre</a>
                    </li>
                </ul>
            </div>
            @guest
            <div>
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('login')}}">Login</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('register')}}">Cadastro</a>
                    </li>
                </ul>
            </div>
            @else
            <div class="btn-group">
                <button class="btn btn-secondary dropdown-toggle text-center" type="button" id="triggerId" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person-circle" viewBox="0 0 16 16">
                        <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z"/>
                        <path fill-rule="evenodd" d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z"/>
                      </svg>
                      {{Auth::user()->name}}
                </button>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="triggerId">
                    <a id="logout" class="dropdown-item" href="#">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-box-arrow-right" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M10 12.5a.5.5 0 0 1-.5.5h-8a.5.5 0 0 1-.5-.5v-9a.5.5 0 0 1 .5-.5h8a.5.5 0 0 1 .5.5v2a.5.5 0 0 0 1 0v-2A1.5 1.5 0 0 0 9.5 2h-8A1.5 1.5 0 0 0 0 3.5v9A1.5 1.5 0 0 0 1.5 14h8a1.5 1.5 0 0 0 1.5-1.5v-2a.5.5 0 0 0-1 0v2z"/>
                            <path fill-rule="evenodd" d="M15.854 8.354a.5.5 0 0 0 0-.708l-3-3a.5.5 0 0 0-.708.708L14.293 7.5H5.5a.5.5 0 0 0 0 1h8.793l-2.147 2.146a.5.5 0 0 0 .708.708l3-3z"/>
                          </svg>Logout </a>
                    <form id="logout-form" method="POST" action="{{route('logout')}}" style="display: none;">
                        @csrf
                    </form>
                </div>
            </div>
            @endguest
        </nav>
    </header>
    <main>
        <div>
            @yield('content')
        </div>
    </main>

    <script>
        const button = document.getElementById('logout');
        const logoutform = document.getElementById('logout-form');
        button?.addEventListener('click', e => {
            e.preventDefault();
            logoutform.submit();
        });
    </script>
</body>
</html>