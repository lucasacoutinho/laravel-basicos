@extends('layouts.master')

@section('pagina', 'Contato')

@section('content')

<div class="container">
    <h3 class="text-center text-uppercase">sobre</h3>
    <p class="text-center w-75 m-auto">Projeto simples utilizando o laravel 8 seguindo os ensinamentos do curso <a class="text-success" href="https://www.udemy.com/course/laravel-beginner-fundamentals/learn/lecture/23329924#overview">Master Laravel for Beginners & Intermediate [2020 Update]</a>

    </p>
    <div class="row">
      <div class="col-sm-12 col-md-6 col-lg-3 my-5">
        <div class="card border-0">
           <div class="card-body text-center">
             <i class="fa fa-phone fa-5x mb-3" aria-hidden="true"></i>
             <h4 class="text-uppercase mb-5">Telefone</h4>
             
             @can('about:show')
              <p>+5538988131321, +5538988131321</p>
             @endcan
             @cannot('about:show')
             <p>+5538988776655, +5538988776655</p>
            @endcan
           </div>
         </div>
      </div>
      <div class="col-sm-12 col-md-6 col-lg-3 my-5">
        <div class="card border-0">
           <div class="card-body text-center">
             <i class="fa fa-map-marker fa-5x mb-3" aria-hidden="true"></i>
             <h4 class="text-uppercase mb-5">Localização</h4>
            <address>39408-354 - Melo, Montes Claros - MG </address>
           </div>
         </div>
      </div>
      <div class="col-sm-12 col-md-6 col-lg-3 my-5">
        <div class="card border-0">
           <div class="card-body text-center">
             <i class="fa fa-map-marker fa-5x mb-3" aria-hidden="true"></i>
             <h4 class="text-uppercase mb-5">Localização</h4>
             <address>39408-354 - Melo, Montes Claros - MG </address>
           </div>
         </div>
      </div>
      <div class="col-sm-12 col-md-6 col-lg-3 my-5">
        <div class="card border-0">
           <div class="card-body text-center">
             <i class="fa fa-globe fa-5x mb-3" aria-hidden="true"></i>
             <h4 class="text-uppercase mb-5">Email</h4>
             @can('about:show')
             <p>luca.coutinho1@gmail.com ldrivefaculdade@gmail.com</p>
            @endcan
            @cannot('about:show')
            <p>example@example.net, example@example.net</p>
            @endcan
           </div>
         </div>
      </div>
    </div>
</div>
@endsection