@extends('layouts.master')

@section('pagina', 'Pagina inicial')

@section('content')
    <div class="jumbotron">
        <h1 class="display-4">Bem vindo 
            @auth
                {{Auth::user()->name}}
            @else
                ao <span>B-Logger</span></h1>
            @endauth 
        <p class="lead"></p>
        <hr class="my-2">
        <p class="lead">
            <a class="btn btn-primary btn-lg" href="{{route('home.about')}}" role="button">Saber mais</a>
        </p>
    </div>
@endsection