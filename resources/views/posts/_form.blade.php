<div class="mb-3 form-group">
    <label for="post-title" class="form-label font-weight-bold">Titulo</label>
    <input  type="text" id="post-title" name="title"  class="form-control" placeholder="Um gavião voo" value="{{old('title')}}">
</div>

<div class="mb-3 form-group">
    <label for="post-content" class="form-label font-weight-bold">Conteúdo</label>
    <textarea id="post-content" name="content" class="form-control" minlength="20" rows="3">{{old('content')}}</textarea>
</div>

<div class="mb-3 form-group">
    <label for="post-thumbnail" class="form-label font-weight-bold">Thumbnail</label>
    <input id="post-thumbnail" type="file" name="thumbnail" class="form-control-file">
</div>