@extends('layouts.master')

@section('pagina', 'Postagens')

@section('content')

<div class="container">
    <div>
        <h1 class="text-muted font-weight-bold">Create a new post</h1>
    </div>
    <x-error :errors="$errors" />
    <div class="bg-white p-3 rounded shadow-sm">
        <form action="{{route('post.store')}}" method="POST" enctype="multipart/form-data">
            @csrf
            @include('posts._form')
            <div class="d-inline-flex">
                <a href="{{route('post.index')}}" type="button"
                    class="btn btn-secondary shadow-sm mr-2 text-capitalize">close</a>
                <x-button type="submit" :message="'Create'" />
            </div>
        </form>
    </div>
</div>

@endsection