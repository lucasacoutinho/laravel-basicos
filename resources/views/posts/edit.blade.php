@extends('layouts.master')

@section('pagina', $post["title"])

@section('content')
<div class="container">
    <div>
        <h5 id="titulo" class="text-muted font-weight-bold">Atualizar postagem</h5>
        <h1 class="text font-weight-light text-dark">{{$post['title']}}</h1>
    </div>
    <x-error :errors="$errors" />
    <div class="bg-white p-3 rounded shadow-sm">
        <form id="novo-post" class="d-grid gap-3" action="{{route('post.update', $post->id)}}" method="POST" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            @include('posts._form')
            <div class="d-inline-flex">
                <a href="{{route('post.index')}}" type="button" class="btn btn-secondary shadow-sm mr-2 text-capitalize">close</a>
                <x-button type="submit" :message="'Save Changes'"/>
            </div>
        </form>
    </div>
</div>
@endsection