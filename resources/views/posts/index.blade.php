@extends('layouts.master')

@section('pagina', 'Postagens')

@section('content')
<div class="container gap-2">
    @auth
        <div class="mb-2"><a type="button" class="btn btn-primary" href="{{route('post.create')}}">Nova postagem</a></div>
    @endauth

    <div class="row">
        <div class="@if (isset($most_commented) || isset($most_active_users) )col-10 @else col-12 @endif d-grid gap-3">
            @forelse ($posts as $post)
            @include('posts.partials.post')
            <br>
            @empty
                <div class="col">
                    <h1>Nenhum post encontrado!</h1>
                </div>
            @endforelse
        </div>
        @if (isset($most_commented) || isset($most_active_users) )
            <div class="col-2 justify-content-start">
                <div class="container">
                    @if (isset($most_commented))    
                        <div class="card shadow-sm mb-2" style="width: 18rem;">
                            <div class="card-body">
                                <h5 class="card-title"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chat-left-text" viewBox="0 0 16 16">
                                    <path d="M14 1a1 1 0 0 1 1 1v8a1 1 0 0 1-1 1H4.414A2 2 0 0 0 3 11.586l-2 2V2a1 1 0 0 1 1-1h12zM2 0a2 2 0 0 0-2 2v12.793a.5.5 0 0 0 .854.353l2.853-2.853A1 1 0 0 1 4.414 12H14a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/>
                                    <path d="M3 3.5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zM3 6a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9A.5.5 0 0 1 3 6zm0 2.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5z"/>
                                </svg> Mais comentados</h5>
                                <p class="card-text font-weight-bold">Postagens mais comentadas</p>
                            </div>
                            <ul class="list-group list-group-flush">
                                @foreach ($most_commented as $key => $value)
                                <li class="list-group-item">
                                    <a href="{{route('post.show', $value->id)}}" class="text-dark">
                                        {{$value->title}}
                                    </a>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if (isset($most_active_users))    
                        <div class="card shadow-sm" style="width: 18rem;">
                            <div class="card-body">
                                <h5 class="card-title"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-graph-up" viewBox="0 0 16 16">
                                    <path fill-rule="evenodd" d="M0 0h1v15h15v1H0V0zm10 3.5a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-1 0V4.9l-3.613 4.417a.5.5 0 0 1-.74.037L7.06 6.767l-3.656 5.027a.5.5 0 0 1-.808-.588l4-5.5a.5.5 0 0 1 .758-.06l2.609 2.61L13.445 4H10.5a.5.5 0 0 1-.5-.5z"/>
                                </svg> Usuarios mais ativos</h5>
                                <p class="card-text font-weight-bold">Os usuarios com maior atividade no ultimo mês</p>
                            </div>
                            <ul class="list-group list-group-flush">
                                @foreach ($most_active_users as $key => $value)
                                <li class="list-group-item ">
                                    {{$value->name}}
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
            </div>
        @endif
    </div>
    <div class="d-flex justify-content-center items-center">
        {{ $posts->links() }}
    </div>

</div>
@endsection