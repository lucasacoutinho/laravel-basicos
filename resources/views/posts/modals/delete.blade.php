<div class="modal fade" id="excluirPostModal" tabindex="-1" role="dialog" aria-labelledby="excluirPostModalTitleId" aria-hidden="true">
    <form action="{{route('post.destroy', $post['id'])}}" method="POST">
        @csrf
        @method('DELETE')
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="excluir-post-modal-title">Excluir postagem</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="d-flex align-items-center justify-content-center p-4">
                        <svg xmlns="http://www.w3.org/2000/svg" width="188" height="188" fill="red" class="bi bi-exclamation-octagon-fill" viewBox="0 0 16 16">
                            <path d="M11.46.146A.5.5 0 0 0 11.107 0H4.893a.5.5 0 0 0-.353.146L.146 4.54A.5.5 0 0 0 0 4.893v6.214a.5.5 0 0 0 .146.353l4.394 4.394a.5.5 0 0 0 .353.146h6.214a.5.5 0 0 0 .353-.146l4.394-4.394a.5.5 0 0 0 .146-.353V4.893a.5.5 0 0 0-.146-.353L11.46.146zM8 4c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 4.995A.905.905 0 0 1 8 4zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z" />
                        </svg>
                    </div>
                    <h6>Tem certeza que deseja excluir <strong>{{$post['title']}}</strong> ?<h6>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary text-capitalize" data-dismiss="modal">fechar</button>
                    <x-button type="submit" :message="'excluir'" class="btn-danger text-capitalize"/>
                </div>
            </div>
        </div>
    </form>
</div>