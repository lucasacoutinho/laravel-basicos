<div class="card shadow-sm">
    <div class="card-body">
        <h5 class="card-title font-weight-bold @if($post->trashed()) text-muted @endif">{{$post->id}}. {{ $post->title }} @if(\Carbon\Carbon::parse($post->created_at)->between(\Carbon\Carbon::yesterday(), now()))<span class="badge badge-danger">New</span> @endif</h5>
        <x-tags :tags="$post->tags"/>
        <p class="card-text">{{ \Illuminate\Support\Str::limit($post->content, 20) }}</p>
        <div class="btn-group" style="float: left;">
            <p class="text-muted @if($post->comments->where('user_id', Auth::id())->count()) font-weight-bold @endif">{{$post->comments->count()}} @if($post->comments->count() === 1) comentário  @else comentários @endif</p>
        </div>
        <div class="btn-group" style="float: right;">
            @auth
                @can ('posts:destroy', $post) 
                    @if(!$post->trashed())
                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#excluirPostModal">Excluir</button>
                    @include('posts.modals.delete')
                    @endif
                @endcan
                @can('posts:update', $post)              
                    <a href="{{route('post.edit', $post->id)}}" class="col btn btn-secondary mx-1 rounded">Editar</a>
                @endcan
            @endauth
            <a href="{{route('post.show', $post->id)}}" class="col btn btn-primary mx-1 rounded">Acessar</a>
        </div>
    </div>
</div>