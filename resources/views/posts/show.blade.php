@extends('layouts.master')

@section('pagina', $post->title)

@section('content')
    <div class="container">
        <x-alert :message="session('status')"/>
        <div>
            <div class="d-flex shadow-sm bg-white p-3 mb-2 rounded p-3 m-3">
                <article class="blog-post">
                    <h1 class="blog-post-title text-weight-bold display-4">{{ $post->title }}</h1>
                    <p class="blog-post-meta text-muted">{{ $post->created_at->diffForHumans() }}, by <a href="{{route('users.show', ['user' => $post->user->id])}}" class="text-success">{{ $post->user->name }}</a></p>
                    <x-tags :tags="$post->tags"/>
                    <div class="d-flex my-4">
                        @if ($post->image)
                            <img src="{{$post->image->image_url}}" alt="{{$post->title}}" class="img-thumbnail img-fluid" width="300" height="400">
                        @endif
                        <p class="lead p-3 ml-3">{{ $post->content }}</p>
                    </div>

                    <div class="text-muted">Numero de acessos: {{$counter}}</div>
                </article>
            </div>
        </div>
        <div>
            @auth
                <div class="shadow-sm bg-white p-3 rounded m-3">
                    <form method="POST" action="{{ route('comment.post.store', $post->id) }}">
                        @include('comments.create')
                    </form>
                </div>
            @endauth
            @if ($post->comments->isNotEmpty())
                @include('comments.index')
            @else
                <div class="d-flex shadow-sm bg-white p-3 rounded m-3 ">
                    <article class="blog-post">
                        <p>Nenhum comentário encontrado!</p>
                    </article>
                </div>
            @endif
        </div>
    </div>
@endsection

<style>

</style>