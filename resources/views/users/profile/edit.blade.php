@extends('layouts.master')
@section('pagina', 'Profile')

@section('content')
    <div class="container">
        <x-alert :message="session('status')"/>
        <x-error :errors="$errors"/>
        <div class="bg-white rounded p-2 shadow-sm">
            <form method="POST" action="{{route('users.update', ['user' => $user->id])}}" class="form-horizontal" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="col-4">
                        <img src="{{$user->image ? $user->image->image_url : ''}}" alt="{{$user->name}}" class="img-thumbnail avatar"/>
    
                        <div class="card mt-4">
                            <div class="card-body">
                                <h6>Upload a different foto</h6>
                                <input type="file" name="avatar" class="form-control-file">
                            </div>
                        </div>
                    </div>
                    <div class="col-8">
                        <div class="form-group">
                          <label for="name">Name:</label>
                          <input type="text" name="name" id="name" class="form-control"/>
                        </div>
                        <div class="form-group">
                            <x-button type="submit" :message="'Save Changes'" />
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection