@extends('layouts.master')
@section('pagina', 'Profile')

@section('content')
<div class="container">
    <div class="bg-white p-3 m-3 rounded-sm shadow-sm">
        <div class="row">
            <div class="col-4">
                <img src="{{$user->image ? $user->image->image_url : ''}}" alt="{{$user->name}}"
                    class="img-thumbnail avatar" />
            </div>
            <div class="col-8 justify-content-center align-content-center">
                <h3 class="text-muted">{{ $user->name }}</h3>
            </div>
        </div>
    </div>
    @auth
        <div class="shadow-sm bg-white p-3 rounded m-3">
            <form method="POST" action="{{ route('comment.user.store', $user->id) }}">
                @include('comments.create')
            </form>
        </div>
    @endauth
    @if ($user->commentsOn->isNotEmpty())
        @include('comments.userIndex')
    @else
    <div class="d-flex shadow-sm bg-white p-3 rounded m-3 ">
        <article class="blog-post">
            <p>Nenhum comentário encontrado!</p>
        </article>
    </div>
    @endif
</div>
@endsection