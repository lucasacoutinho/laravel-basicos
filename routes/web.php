<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\PostTagController;
use App\Http\Controllers\UserCommentController;

Route::get('/', [HomeController::class, 'home'])->name('home.index');
Route::get('/about',  [HomeController::class, 'about'])->name('home.about');
Route::resource('/post', PostController::class);
Auth::routes();

Route::post('/post/{post}/comment',             [CommentController::class, 'store'])->name('comment.post.store');
Route::put('/post/{post}/comment/{comment}',    [CommentController::class, 'update'])->name('comment.post.update');
Route::delete('/post/{post}/comment/{comment}', [CommentController::class, 'destroy'])->name('comment.post.destroy');

Route::get('/post/tag/{tag}', [PostTagController::class, 'index'])->name('post.tags.index');

Route::resource('/users', UserController::class)->only(['show', 'edit', 'update']);
Route::post('/users/{user}/comment/create', [UserCommentController::class, 'store'])->name('comment.user.store')->middleware('auth');

Route::get('/mailable', function () {
    $comment = Cache::remember('mail-template', now()->addSeconds(3600), fn () => \App\Models\Comment::inRandomOrder()->first());
    return new \App\Mail\CommentPostedMarkdown($comment);
});