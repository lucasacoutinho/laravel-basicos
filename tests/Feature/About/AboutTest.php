<?php

namespace Tests\Feature\About;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AboutTest extends TestCase
{
    public function test_about_is_working()
    {
        $response = $this->get('/about');

        $response->assertSeeText('Projeto simples utilizando o laravel 8 seguindo os ensinamentos do curso');
    }
}
