<?php

namespace Tests\Feature\Blog\Post;

use Tests\TestCase;
use App\Models\User;
use App\Models\BlogPost;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DestroyTest extends TestCase
{
    use RefreshDatabase;

    public function test_delete_blog_post()
    {
        $user = User::factory()->create();
        $post = BlogPost::factory()->for($user , 'user')->create();
        $this->assertDatabaseHas('blog_posts', $post->toArray());
        
        $response = $this->actingAs($user)->delete("post/{$post->id}");
        
        $response->assertStatus(302)->assertSessionHas('status');

        $this->assertEquals(session('status'), "Postagem $post->title excluida com sucesso!");
        // $this->assertDatabaseMissing('blog_posts', $post->toArray());
        $this->assertSoftDeleted('blog_posts', $post->toArray());
    }
}
