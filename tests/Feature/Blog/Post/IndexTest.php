<?php

namespace Tests\Feature\Blog\Post;

use Tests\TestCase;
use App\Models\User;
use App\Models\Comment;
use App\Models\BlogPost;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class IndexTest extends TestCase
{
    use RefreshDatabase;
    
    public function test_no_posts_when_nothing_on_database() {
        $response = $this->get('/post');

        $response->assertSeeText('Nenhum post encontrado!');
    }

    public function test_see_blog_post() {  
        $post = new BlogPost();
        $post->title = 'New Title';
        $post->content = 'ASKDALSDJALKDJALSKDJALSDQWJKLENQWEKQWNQKWJENQWKEJNQK';
        $post->user()->associate(User::factory()->create());
        $post->save();

        $response = $this->get('/post');

        $response->assertSeeText('New Title');

        $this->assertDatabaseHas('blog_posts', [
            'title' => 'New Title',
        ]);
    }

    public function test_store_with_comments() {
        $user = User::factory()->create();

        $post = BlogPost::factory()->for($user, 'user')->create();

        Comment::factory(3)->for($user, 'user')->create([
            'commentable_id'   => $post->id,
            'commentable_type' => 'App\Models\BlogPost'
        ]);

        $response = $this->get('/post');

        $response->assertSeeText('3  comentários');
    }
}
