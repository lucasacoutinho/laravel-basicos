<?php

namespace Tests\Feature\Blog\Post;

use Tests\TestCase;
use App\Models\User;
use App\Models\BlogPost;
use Illuminate\Support\Str;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UpdateTest extends TestCase
{
    use RefreshDatabase;

    public function test_update_blog_post()
    {
        $user = User::factory()->create();

        $post = BlogPost::factory()->for($user, 'user')->create();
        $this->assertDatabaseHas('blog_posts', $post->toArray());

        $data = BlogPost::factory()->make()->toArray();

        $response = $this->actingAs($user)->put("post/{$post->id}", $data);

        $response->assertStatus(302)->assertSessionHas('status');

        $this->assertEquals(session('status'), 'Postagem atualizada com sucesso!');
        $this->assertDatabaseMissing('blog_posts', $post->toArray());
        $this->assertDatabaseHas('blog_posts', $data);
    }

    public function test_store_validation_required()
    {
        $user = User::factory()->create();

        $post = BlogPost::factory()->for($user, 'user')->create();
        $this->assertDatabaseHas('blog_posts', $post->toArray());

        $data = [
            'title'   => '',
            'content' => '',
        ];

        $response = $this->actingAs($user)->put("post/{$post->id}", $data);

        $response->assertStatus(302)->assertSessionHas('errors');
        $messages = session('errors')->getMessages();
        $this->assertEquals($messages['title'][0], "The title field must have a value.");
        $this->assertEquals($messages['content'][0], "The content field must have a value.");
    }

    public function test_store_validation_size()
    {
        $user = User::factory()->create();

        $post = BlogPost::factory()->for($user, 'user')->create();
        $this->assertDatabaseHas('blog_posts', $post->toArray());

        $data = [
            'title'   => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
            'content' => Str::length(5),
        ];

        $response = $this->actingAs($user)->put("post/{$post->id}", $data);

        $response->assertStatus(302)->assertSessionHas('errors');
        $messages = session('errors')->getMessages();

        $this->assertEquals($messages['title'][0], "The title must not be greater than 255 characters.");
        $this->assertEquals($messages['content'][0], "The content must be at least 20 characters.");
    }
}
