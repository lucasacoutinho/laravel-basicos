<?php

namespace Tests\Feature\Home;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class HomeTest extends TestCase
{
    public function test_home_is_working()
    {
        $response = $this->get('/');

        $response->assertSeeText('Bem vindo');
    }
}
